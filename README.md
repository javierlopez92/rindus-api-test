# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Assumptions ###

This API has been developed following the following assumptions:

* CRUD operations for *posts* and *users* resources of https://jsonplaceholder.typicode.com/.
* To simplify, *users* attributes (address and company) have been modify to reduce complexity, replacing the JSON Object with a String.
* Given there is no persistence requirement, the data is imported from a JSON file and stored in collections in the beginning of the execution.

### Framework and libraries ###

* SpringBoot 2.5.1 (devtools + starter-web)
* Java 11.
* Springdoc-openapi v1.5.9

### Code functionality ###

* REST API with CRUD operations.
* Exception handling.
* Code tested.
* Logs.
* Swagger definition.

### Next steps ###

* Persist data and include Spring Data.
* Add more unique restrictions (username, email...).
* Securize the API with Spring Security (OAUTH 2).
* Prepare for a microservice approach with Spring Cloud (Eureka, Feign, Ribbon...)

### API Definition ###

Available through http://localhost:8080/swagger-ui/index.html

![Swagger definition](https://i.ibb.co/C9FLhfb/swagger.png)