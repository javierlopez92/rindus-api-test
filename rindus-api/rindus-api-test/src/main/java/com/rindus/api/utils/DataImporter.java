package com.rindus.api.utils;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rindus.api.models.entity.Post;
import com.rindus.api.models.entity.User;

public class DataImporter {
	
	public static List<User> importUsers(File usersFile) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        User[] users = objectMapper.readValue(usersFile, User[].class);
        return Arrays.asList(users);
	}
	
	public static List<Post> importPosts(File postsFile) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Post[] posts = objectMapper.readValue(postsFile, Post[].class);
        return Arrays.asList(posts);
	}
}
