package com.rindus.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RindusApiTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RindusApiTestApplication.class, args);
	}

}
