package com.rindus.api.exceptions;

public class IdAlreadyInUseException extends Exception {

	private static final long serialVersionUID = -6618013376655636611L;
	
	private static final String message = "ID already in use";
	
	public IdAlreadyInUseException() {
		super(message);
	}

}
