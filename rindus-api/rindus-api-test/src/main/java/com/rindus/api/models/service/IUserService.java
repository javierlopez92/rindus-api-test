package com.rindus.api.models.service;

import java.util.List;

import com.rindus.api.exceptions.IdAlreadyInUseException;
import com.rindus.api.exceptions.UserNotFoundException;
import com.rindus.api.models.entity.User;

public interface IUserService {
	
	public List<User> findAll();
	public User findById(Long id) throws UserNotFoundException;
	public User createUser(User user) throws IdAlreadyInUseException;
	public User updateUser(User user) throws UserNotFoundException, IdAlreadyInUseException;
	public void deleteUserById(Long id) throws UserNotFoundException;

}
