package com.rindus.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rindus.api.exceptions.IdAlreadyInUseException;
import com.rindus.api.exceptions.PostNotFoundException;
import com.rindus.api.exceptions.UserNotFoundException;
import com.rindus.api.models.entity.Post;
import com.rindus.api.models.service.IPostService;

@RestController
public class PostController {
	
	@Autowired
	IPostService postService;
	
	/* Posts Endpoints */
	@GetMapping("/posts")
	public List<Post> getPosts() {
		return postService.findAll();
	}
	
	@GetMapping("/posts/{id}")
	public Post getPostById(@PathVariable Long id) throws PostNotFoundException {
		return postService.findById(id);
	}
	
	@GetMapping("/postsByUserId/{userId}")
	public List<Post> getPostsByUserId(@PathVariable Long userId) throws UserNotFoundException {
		return postService.findByUserId(userId);
	}
	
	@PostMapping("/createPost")
	@ResponseStatus(HttpStatus.CREATED)
	public Post createPost(@RequestBody Post post) throws IdAlreadyInUseException, UserNotFoundException {
		return postService.createPost(post);
	}
	
	@PutMapping("/editPost")
	@ResponseStatus(HttpStatus.CREATED)
	public Post editPost(@RequestBody Post post) throws PostNotFoundException, UserNotFoundException {
		return postService.updatePost(post);
	}
	
	@DeleteMapping("/deletePost/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletePost(@PathVariable Long id) throws PostNotFoundException {
		postService.deletePostById(id);
	}
}
