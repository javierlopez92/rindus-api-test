package com.rindus.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.rindus.api.exceptions.IdAlreadyInUseException;
import com.rindus.api.exceptions.UserNotFoundException;
import com.rindus.api.models.entity.User;
import com.rindus.api.models.service.IUserService;

@RestController
public class UserController {
	
	@Autowired
	IUserService userService;
	
	/* Users Endpoints */
	@GetMapping("/users")
	public List<User> getUsers() {
		return userService.findAll();
	}
	
	@GetMapping("/users/{id}")
	public User getUserById(@PathVariable Long id) throws UserNotFoundException {
		return userService.findById(id);
	}
	
	@PostMapping("/createUser")
	@ResponseStatus(HttpStatus.CREATED)
	public User createBook(@RequestBody User user) throws IdAlreadyInUseException {
		return userService.createUser(user);
	}
	
	@PutMapping("/editUser")
	@ResponseStatus(HttpStatus.CREATED)
	public User editBook(@RequestBody User user) throws UserNotFoundException, IdAlreadyInUseException {
		return userService.updateUser(user);
	}

	@DeleteMapping("/deleteUser/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUser(@PathVariable Long id) throws UserNotFoundException {
		userService.deleteUserById(id);
	}
}
