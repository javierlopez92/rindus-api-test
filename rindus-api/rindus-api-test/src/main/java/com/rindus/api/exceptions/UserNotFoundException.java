package com.rindus.api.exceptions;

public class UserNotFoundException extends Exception {

	private static final long serialVersionUID = -6618013376655636611L;
	
	private static final String message = "User not found";
	
	public UserNotFoundException() {
		super(message);
	}

}
