package com.rindus.api.exceptions;

public class PostNotFoundException extends Exception {

	private static final long serialVersionUID = -6618013376655636611L;
	
	private static final String message = "Post not found";
	
	public PostNotFoundException() {
		super(message);
	}

}
