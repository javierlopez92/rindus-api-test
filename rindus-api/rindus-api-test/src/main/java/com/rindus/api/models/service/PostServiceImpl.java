package com.rindus.api.models.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.rindus.api.exceptions.IdAlreadyInUseException;
import com.rindus.api.exceptions.PostNotFoundException;
import com.rindus.api.exceptions.UserNotFoundException;
import com.rindus.api.models.entity.Post;
import com.rindus.api.models.entity.User;
import com.rindus.api.utils.DataImporter;

@Service
public class PostServiceImpl implements IPostService {
	
	private static Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);
	
	private List<Post> posts = new ArrayList<>();

	@Value("classpath:data/posts.json")
	private Resource postsFile;
	
	@Autowired
	IUserService userServce;
	
	@PostConstruct
	public void importPosts() {
		try {
			logger.debug("Importing posts from file");
			
			//Clear posts
			this.posts.clear();
			
			//Import valid posts from file
			DataImporter.importPosts(postsFile.getFile()).stream()
				.filter(p->isValidPost(p))
				.forEach(p->this.posts.add(p));
			
			logger.debug("Posts imported: {}", this.posts.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public List<Post> findAll() {
		logger.debug("Getting all posts");
		return this.posts;
	}
	
	@Override
	public Post findById(Long id) throws PostNotFoundException {
		logger.debug("Trying to get post with ID {}", id);
		return this.posts.stream().filter(p->p.getId().equals(id)).findFirst().orElseThrow(PostNotFoundException::new);
	}
	
	@Override
	public Post createPost(Post post) throws IdAlreadyInUseException, UserNotFoundException {
		logger.debug("Trying to edit post {}", post);

		//Checks
		checkIdAlreadyInUse(post.getId());
		try {
			this.userServce.findById(post.getUserId());
		} catch (UserNotFoundException e) {
			logger.error("User not found with ID {}, cannot create post", post.getUserId());
			throw e;
		}
		
		//Create
		this.posts.add(post);
		logger.info("Post created");
		return post;
	}

	@Override
	public Post updatePost(Post post) throws PostNotFoundException, UserNotFoundException {
		logger.debug("Trying to edit post {}" + post);

		//Check if post and user exist
		Post postToEdit;
		try {
			postToEdit = findById(post.getId());
			this.userServce.findById(post.getUserId());
		} catch (PostNotFoundException e1) {
			logger.error("Post not found for ID {}, cannot edit post", post.getId());
			throw e1;
		} catch (UserNotFoundException e2) {
			logger.error("User not found for ID {}, cannot edit post", post.getUserId());
			throw e2;
		}

		//Replace post
		this.posts.remove(postToEdit);
		this.posts.add(post);
		logger.info("Post edited");
		return post;
	}

	@Override
	public void deletePostById(Long id) throws PostNotFoundException {
		logger.debug("Trying to delete post with ID {}", id);

		try {
			Post postToRemove = findById(id);
			this.posts.remove(postToRemove);		
			logger.info("Post deleted");
		} catch (PostNotFoundException e) {
			logger.error("Post not found for ID {}, cannot delete post", id);
			throw e;
		}
	}

	@Override
	public List<Post> findByUserId(Long userId) throws UserNotFoundException {
		User user = userServce.findById(userId);
		return this.posts.stream().filter(p->p.getUserId().equals(user.getId())).collect(Collectors.toList());
	}
	
	private void checkIdAlreadyInUse(Long id) throws IdAlreadyInUseException {
		if (this.posts.stream().anyMatch(p->p.getId().equals(id))) {
			logger.error("User ID {} already in use", id);
			throw new IdAlreadyInUseException();
		}
	}
	
	private boolean isValidPost(Post post) {
		try {
			checkIdAlreadyInUse(post.getId());
			this.userServce.findById(post.getUserId());
			return true;
		} catch (UserNotFoundException e) {
			return false;
		} catch (IdAlreadyInUseException e) {
			return false;
		}
	}
}
