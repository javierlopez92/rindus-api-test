package com.rindus.api.models.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.rindus.api.exceptions.IdAlreadyInUseException;
import com.rindus.api.exceptions.UserNotFoundException;
import com.rindus.api.models.entity.User;
import com.rindus.api.utils.DataImporter;

@Service
public class UserServiceImpl implements IUserService {
	
	private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Value("classpath:data/users.json")
	private Resource usersFile;
	
	private List<User> users = new ArrayList<>();
	
	@PostConstruct
	public void importUsers() {
		try {
			logger.debug("Importing users from file");
			
			//Clear users
			this.users.clear();
			
			//Import valid users from file
			DataImporter.importUsers(usersFile.getFile()).stream()
				.filter(u->isValidUser(u))
				.forEach(u->this.users.add(u));

			logger.debug("Users imported: {}", this.users.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<User> findAll() {
		logger.debug("Getting all users");
		return this.users;
	}

	@Override
	public User findById(Long id) throws UserNotFoundException {
		logger.debug("Trying to get user with ID {}", id);
		return this.users.stream().filter(u->u.getId().equals(id)).findFirst().orElseThrow(UserNotFoundException::new);
	}
	
	@Override
	public User createUser(User user) throws IdAlreadyInUseException {
		logger.debug("Trying to create user {}", user);
		checkIdAlreadyInUse(user.getId());
		this.users.add(user);
		logger.info("User created");
		return user;
	}

	@Override
	public User updateUser(User user) throws IdAlreadyInUseException, UserNotFoundException {
		logger.debug("Trying to edit user {}" + user);
	
		User userToEdit;
		try {
			userToEdit = findById(user.getId());
		} catch (UserNotFoundException e) {
			logger.error("Cannot edit user with ID {}", user.getId());
			throw e;
		}
			
		
		if (!user.getUsername().equals(userToEdit.getUsername())) {
			//Avoid the edition of a different user using a wrong ID but already in use
			logger.error("Cannot edit the username of the user");
			throw new IdAlreadyInUseException();
		}
		
		//Replace user
		this.users.remove(userToEdit);
		this.users.add(user);
		logger.info("User edited");
		return user;
	}

	@Override
	public void deleteUserById(Long id) throws UserNotFoundException {
		logger.debug("Trying to delete user with ID {}", id);
		
		try {
			User userToRemove = findById(id);
			this.users.remove(userToRemove);
			logger.info("User deleted");
		} catch (UserNotFoundException e) {
			logger.error("User not found for ID {}, cannot delete user", id);
			throw e;
		}
		
	}
	
	private void checkIdAlreadyInUse(Long id) throws IdAlreadyInUseException {
		if (this.users.stream().anyMatch(u->u.getId().equals(id))) {
			logger.error("User ID {} already in use", id);
			throw new IdAlreadyInUseException();
		}
	}
	
	private boolean isValidUser(User user) {
		try {
			checkIdAlreadyInUse(user.getId());
			return true;
		} catch (IdAlreadyInUseException e) {
			return false;
		}
	}

}
