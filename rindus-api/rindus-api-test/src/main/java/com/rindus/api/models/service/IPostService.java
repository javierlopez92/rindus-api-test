package com.rindus.api.models.service;

import java.util.List;

import com.rindus.api.exceptions.IdAlreadyInUseException;
import com.rindus.api.exceptions.PostNotFoundException;
import com.rindus.api.exceptions.UserNotFoundException;
import com.rindus.api.models.entity.Post;

public interface IPostService {
	
	public List<Post> findAll();
	public Post findById(Long id) throws PostNotFoundException;
	public Post createPost(Post post) throws IdAlreadyInUseException, UserNotFoundException;
	public Post updatePost(Post post) throws PostNotFoundException, UserNotFoundException;
	public void deletePostById(Long id) throws PostNotFoundException;
	
	public List<Post> findByUserId(Long userId) throws UserNotFoundException;
}
