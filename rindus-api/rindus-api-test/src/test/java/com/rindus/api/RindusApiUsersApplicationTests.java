package com.rindus.api;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.rindus.api.exceptions.IdAlreadyInUseException;
import com.rindus.api.exceptions.UserNotFoundException;
import com.rindus.api.models.entity.User;
import com.rindus.api.models.service.IUserService;

@SpringBootTest
class RindusApiUsersApplicationTests {

	@Autowired
	IUserService userService;
	
	@Test
	public void createGetDeleteUserOkTest() {
		User newUser = new User(11L, "test-name", "test-username", "test-email", "test-address", "test-phone", "test-website", "test-company");
		try {
			//Creation tests
			userService.createUser(newUser);
			
			//Get tests
			assertTrue(userService.findAll().contains(newUser));
			assertTrue(userService.findById(newUser.getId()).equals(newUser));
			
			//Delete test
			userService.deleteUserById(newUser.getId());
			assertThatThrownBy(() -> userService.findById(newUser.getId())).isInstanceOf(UserNotFoundException.class);
		} catch (Exception e) {
			fail("Wrong behaviour");
		}
	}
	
	@Test
	public void editTestOk() {
		String name1 = "test-name-1";
		String name2 = "test-name-2";
	
		User newUser = new User(11L, name1, "test-username", "test-email", "test-address", "test-phone", "test-website", "test-company");
		try {
			//Create user
			userService.createUser(newUser);
			assertTrue(userService.findById(newUser.getId()).getName().equals(name1));
			
			//Edit user name
			newUser.setName(name2);
			userService.updateUser(newUser);
			assertTrue(userService.findById(newUser.getId()).getName().equals(name2));
			
			//Delete
			userService.deleteUserById(newUser.getId());
			
		} catch (Exception e) {
			fail("Wrong behaviour");
		}
	}
	
	@Test
	public void testCreateNok() {
		try {
			assertNotNull(userService.findById(1L));
		} catch (UserNotFoundException e) {
			fail("Wrong behaviour");
		}
		
		User newUser = new User(1L, "test-name", "test-username", "test-email", "test-address", "test-phone", "test-website", "test-company");
		assertThatThrownBy(() -> userService.createUser(newUser)).isInstanceOf(IdAlreadyInUseException.class);
	}
	
	@Test
	public void testGetNok() {
		assertThatThrownBy(() -> userService.findById(1000L)).isInstanceOf(UserNotFoundException.class);
	}
	
	@Test
	public void testEditNok() {
		User userToEdit = new User(1000L, "test-name", "test-username", "test-email", "test-address", "test-phone", "test-website", "test-company");
		assertThatThrownBy(() -> userService.updateUser(userToEdit)).isInstanceOf(UserNotFoundException.class);
		
		userToEdit.setId(1L);
		assertThatThrownBy(() -> userService.updateUser(userToEdit)).isInstanceOf(IdAlreadyInUseException.class);
	}
	
	@Test
	public void testDeleteNok() {
		assertThatThrownBy(() -> userService.deleteUserById(1000L)).isInstanceOf(UserNotFoundException.class);
	}
}
