package com.rindus.api;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.rindus.api.exceptions.IdAlreadyInUseException;
import com.rindus.api.exceptions.PostNotFoundException;
import com.rindus.api.exceptions.UserNotFoundException;
import com.rindus.api.models.entity.Post;
import com.rindus.api.models.service.IPostService;

@SpringBootTest
class RindusApiPostsApplicationTests {

	@Autowired
	IPostService postService;
	
	@Test
	public void createGetDeletePostOkTest() {
		Post newPost = new Post(111L, "test-title", "test-body", 1L);

		try {
			//Creation tests
			postService.createPost(newPost);
			
			//Get tests
			assertTrue(postService.findAll().contains(newPost));
			assertTrue(postService.findById(newPost.getId()).equals(newPost));
			assertTrue(postService.findByUserId(1L).contains(newPost));
			
			//Delete test
			postService.deletePostById(newPost.getId());
			assertFalse(postService.findByUserId(1L).contains(newPost));
			assertThatThrownBy(() -> postService.findById(newPost.getId())).isInstanceOf(PostNotFoundException.class);
		} catch (Exception e) {
			fail("Wrong behaviour");
		}
	}
	
	@Test
	public void editTestOk() {
		String title1 = "test-name-1";
		String title2 = "test-name-2";
	
		Post newPost = new Post(111L, title1, "test-body", 1L);
		try {
			//Create user
			postService.createPost(newPost);
			assertTrue(postService.findById(newPost.getId()).getTitle().equals(title1));
			
			//Edit user name
			newPost.setTitle(title2);
			postService.updatePost(newPost);
			assertTrue(postService.findById(newPost.getId()).getTitle().equals(title2));
			
			//Delete
			postService.deletePostById(newPost.getId());
			
		} catch (Exception e) {
			fail("Wrong behaviour");
		}
	}
	
	@Test
	public void testCreateNok() {
		try {
			assertNotNull(postService.findById(1L));
		} catch (PostNotFoundException e) {
			fail("Wrong behaviour");
		}
		
		Post newPost1 = new Post(1L, "test-title", "test-body", 1L);
		assertThatThrownBy(() -> postService.createPost(newPost1)).isInstanceOf(IdAlreadyInUseException.class);
		
		Post newPost2 = new Post(111L, "test-title", "test-body", 1000L);
		assertThatThrownBy(() -> postService.createPost(newPost2)).isInstanceOf(UserNotFoundException.class);
	}
	
	@Test
	public void testGetNok() {
		assertThatThrownBy(() -> postService.findById(1000L)).isInstanceOf(PostNotFoundException.class);
		assertThatThrownBy(() -> postService.findByUserId(1000L)).isInstanceOf(UserNotFoundException.class);
	}
	
	@Test
	public void testEditNok() {
		Post postToEdit = new Post(111L, "test-title", "test-body", 1L);
		assertThatThrownBy(() -> postService.updatePost(postToEdit)).isInstanceOf(PostNotFoundException.class);
		
		postToEdit.setId(1L);
		postToEdit.setUserId(1000L);
		assertThatThrownBy(() -> postService.updatePost(postToEdit)).isInstanceOf(UserNotFoundException.class);
	}
	
	@Test
	public void testDeleteNok() {
		assertThatThrownBy(() -> postService.deletePostById(1000L)).isInstanceOf(PostNotFoundException.class);
	}
}
